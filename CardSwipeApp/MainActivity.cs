using Android.App;
using Android.Widget;
using Android.OS;
using CardSwipeLib;
using Android.Views;
using Android.Content;
using Android.Util;
using System;
using Android.Runtime;
using System.Collections.Generic;

namespace CardSwipeApp
{
	[Activity (Label = "CardSwipeApp", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		private List<String> al;
		private ArrayAdapter<String> arrayAdapter;
		SwipeFlingAdapterView flingContainer;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			flingContainer = FindViewById<SwipeFlingAdapterView>(Resource.Id.frame);

			al = new List<String>();
			al.Add("php");
			al.Add("c");
			al.Add("python");
			al.Add("java");
			al.Add("html");
			al.Add("c++");
			al.Add("css");
			al.Add("javascript");

			arrayAdapter = new ArrayAdapter<String> (this, Resource.Layout.item, Resource.Id.helloText, al);

			flingContainer.Adapter = arrayAdapter;


			flingContainer.setFlingListener (new CardSwipeListener (flingContainer,al,arrayAdapter));

		}
		private static float ToRadians (float degrees)
		{
			return (float) (degrees * (System.Math.PI/180.0));
		}
	}

	class CardSwipeListener : SwipeFlingAdapterView.onFlingListener {
		private List<String> al;
		private ArrayAdapter<String> arrayAdapter;
		private int nextCardNumber = 0;
		private SwipeFlingAdapterView flingContainer;

		public CardSwipeListener (SwipeFlingAdapterView flingContainerInstance, 
			List<String> data, ArrayAdapter<String> arrayAdapterInstance)
		{
			flingContainer = flingContainerInstance;
			al = data;
			arrayAdapter=arrayAdapterInstance;
		}
		#region onFlingListener implementation

		public void removeFirstObjectInAdapter ()
		{
			al.RemoveAt(0);
			arrayAdapter.NotifyDataSetChanged ();
		}

		public void onLeftCardExit (object dataObject)
		{
			Toast.MakeText (Application.Context, "Left", ToastLength.Short).Show ();
		}

		public void onRightCardExit (object dataObject)
		{
			Toast.MakeText (Application.Context, "Right", ToastLength.Short).Show ();
		}

		public void onAdapterAboutToEmpty (int itemsInAdapter)
		{
			al.Add("XML " + nextCardNumber.ToString());
			arrayAdapter.NotifyDataSetChanged();
		}

		public void onScroll (float scrollProgressPercent)
		{
			View view = flingContainer.SelectedView;

			View rightIndicator = view.FindViewById<View> (Resource.Id.item_swipe_right_indicator);
			rightIndicator.Alpha = scrollProgressPercent < 0 ? -scrollProgressPercent : 0;
			View leftIndicator = view.FindViewById<View> (Resource.Id.item_swipe_left_indicator);
			leftIndicator.Alpha = scrollProgressPercent > 0 ? scrollProgressPercent : 0;
		}

		#endregion


	}
}


namespace CompoundCustomControlView
{
	public class SwipeFlingAdapterViewDeux : BaseFlingAdapterView {
		#region implemented abstract members of AdapterView

		public override void SetSelection (int position)
		{
			throw new NotImplementedException ();
		}

		public override View SelectedView {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion

		#region implemented abstract members of AdapterView

		public override IListAdapter Adapter {
			get {
				throw new NotImplementedException ();
			}
			set {
				throw new NotImplementedException ();
			}
		}

		#endregion

//		protected SwipeFlingAdapterViewDeux (IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer) {
//			System.Diagnostics.Debug.WriteLine ("here");
//		}
		public SwipeFlingAdapterViewDeux (Context context) :
		base (context)
		{}
		public SwipeFlingAdapterViewDeux (Context context, IAttributeSet attrs) 
			: base (context, attrs)
		{}
		public SwipeFlingAdapterViewDeux (Context context, IAttributeSet attrs, int defStyle) 
			: base (context, attrs, defStyle)
		{}
	}
	public class CompoundCodeView : LinearLayout
	{
		public CompoundCodeView (Context context) :
		base (context)
		{
			Initialize ();
		}

		public CompoundCodeView (Context context, IAttributeSet attrs) :
		base (context, attrs)
		{
			Initialize ();
		}

		public CompoundCodeView (Context context, IAttributeSet attrs, int defStyle) :
		base (context, attrs, defStyle)
		{
			Initialize ();
		}

		void Initialize ()
		{
			// Add custom controls to your view with some code
			TextView tv = new TextView (Context);
			tv.Text = "Text1";

			Button b = new Button (Context);
			b.Text = "Button1";

			AddView (tv);
			AddView (b);
		}
	}
}
