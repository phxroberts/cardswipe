package md5bbf3a1d83c65c10734ac3299993c16ca;


public class SwipeFlingAdapterViewDeux
	extends md5feca4c7b78b30334560e51c2a0f57188.BaseFlingAdapterView
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_setSelection:(I)V:GetSetSelection_IHandler\n" +
			"n_getSelectedView:()Landroid/view/View;:GetGetSelectedViewHandler\n" +
			"n_getAdapter:()Landroid/widget/Adapter;:GetGetAdapterHandler\n" +
			"n_setAdapter:(Landroid/widget/Adapter;)V:GetSetAdapter_Landroid_widget_Adapter_Handler\n" +
			"";
		mono.android.Runtime.register ("CompoundCustomControlView.SwipeFlingAdapterViewDeux, CardSwipeApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", SwipeFlingAdapterViewDeux.class, __md_methods);
	}


	public SwipeFlingAdapterViewDeux (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == SwipeFlingAdapterViewDeux.class)
			mono.android.TypeManager.Activate ("CompoundCustomControlView.SwipeFlingAdapterViewDeux, CardSwipeApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public SwipeFlingAdapterViewDeux (android.content.Context p0, android.util.AttributeSet p1) throws java.lang.Throwable
	{
		super (p0, p1);
		if (getClass () == SwipeFlingAdapterViewDeux.class)
			mono.android.TypeManager.Activate ("CompoundCustomControlView.SwipeFlingAdapterViewDeux, CardSwipeApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public SwipeFlingAdapterViewDeux (android.content.Context p0, android.util.AttributeSet p1, int p2) throws java.lang.Throwable
	{
		super (p0, p1, p2);
		if (getClass () == SwipeFlingAdapterViewDeux.class)
			mono.android.TypeManager.Activate ("CompoundCustomControlView.SwipeFlingAdapterViewDeux, CardSwipeApp, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public void setSelection (int p0)
	{
		n_setSelection (p0);
	}

	private native void n_setSelection (int p0);


	public android.view.View getSelectedView ()
	{
		return n_getSelectedView ();
	}

	private native android.view.View n_getSelectedView ();


	public android.widget.Adapter getAdapter ()
	{
		return n_getAdapter ();
	}

	private native android.widget.Adapter n_getAdapter ();


	public void setAdapter (android.widget.Adapter p0)
	{
		n_setAdapter (p0);
	}

	private native void n_setAdapter (android.widget.Adapter p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
