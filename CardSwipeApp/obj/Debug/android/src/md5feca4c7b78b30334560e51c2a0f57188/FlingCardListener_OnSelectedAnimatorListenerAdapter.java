package md5feca4c7b78b30334560e51c2a0f57188;


public class FlingCardListener_OnSelectedAnimatorListenerAdapter
	extends android.animation.AnimatorListenerAdapter
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onAnimationEnd:(Landroid/animation/Animator;)V:GetOnAnimationEnd_Landroid_animation_Animator_Handler\n" +
			"";
		mono.android.Runtime.register ("CardSwipeLib.FlingCardListener/OnSelectedAnimatorListenerAdapter, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", FlingCardListener_OnSelectedAnimatorListenerAdapter.class, __md_methods);
	}


	public FlingCardListener_OnSelectedAnimatorListenerAdapter () throws java.lang.Throwable
	{
		super ();
		if (getClass () == FlingCardListener_OnSelectedAnimatorListenerAdapter.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.FlingCardListener/OnSelectedAnimatorListenerAdapter, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onAnimationEnd (android.animation.Animator p0)
	{
		n_onAnimationEnd (p0);
	}

	private native void n_onAnimationEnd (android.animation.Animator p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
