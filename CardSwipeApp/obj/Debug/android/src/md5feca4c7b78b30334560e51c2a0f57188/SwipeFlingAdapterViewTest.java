package md5feca4c7b78b30334560e51c2a0f57188;


public class SwipeFlingAdapterViewTest
	extends md5feca4c7b78b30334560e51c2a0f57188.BaseFlingAdapterView
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_getSelectedView:()Landroid/view/View;:GetGetSelectedViewHandler\n" +
			"n_getAdapter:()Landroid/widget/Adapter;:GetGetAdapterHandler\n" +
			"n_setAdapter:(Landroid/widget/Adapter;)V:GetSetAdapter_Landroid_widget_Adapter_Handler\n" +
			"n_onLayout:(ZIIII)V:GetOnLayout_ZIIIIHandler\n" +
			"n_generateLayoutParams:(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;:GetGenerateLayoutParams_Landroid_util_AttributeSet_Handler\n" +
			"";
		mono.android.Runtime.register ("CardSwipeLib.SwipeFlingAdapterViewTest, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", SwipeFlingAdapterViewTest.class, __md_methods);
	}


	public SwipeFlingAdapterViewTest (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == SwipeFlingAdapterViewTest.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterViewTest, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public SwipeFlingAdapterViewTest (android.content.Context p0, android.util.AttributeSet p1) throws java.lang.Throwable
	{
		super (p0, p1);
		if (getClass () == SwipeFlingAdapterViewTest.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterViewTest, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public SwipeFlingAdapterViewTest (android.content.Context p0, android.util.AttributeSet p1, int p2) throws java.lang.Throwable
	{
		super (p0, p1, p2);
		if (getClass () == SwipeFlingAdapterViewTest.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterViewTest, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public android.view.View getSelectedView ()
	{
		return n_getSelectedView ();
	}

	private native android.view.View n_getSelectedView ();


	public android.widget.Adapter getAdapter ()
	{
		return n_getAdapter ();
	}

	private native android.widget.Adapter n_getAdapter ();


	public void setAdapter (android.widget.Adapter p0)
	{
		n_setAdapter (p0);
	}

	private native void n_setAdapter (android.widget.Adapter p0);


	public void onLayout (boolean p0, int p1, int p2, int p3, int p4)
	{
		n_onLayout (p0, p1, p2, p3, p4);
	}

	private native void n_onLayout (boolean p0, int p1, int p2, int p3, int p4);


	public android.view.ViewGroup.LayoutParams generateLayoutParams (android.util.AttributeSet p0)
	{
		return n_generateLayoutParams (p0);
	}

	private native android.view.ViewGroup.LayoutParams n_generateLayoutParams (android.util.AttributeSet p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
