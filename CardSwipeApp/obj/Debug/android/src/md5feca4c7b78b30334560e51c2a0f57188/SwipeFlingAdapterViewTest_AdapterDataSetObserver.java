package md5feca4c7b78b30334560e51c2a0f57188;


public class SwipeFlingAdapterViewTest_AdapterDataSetObserver
	extends android.database.DataSetObserver
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onChanged:()V:GetOnChangedHandler\n" +
			"n_onInvalidated:()V:GetOnInvalidatedHandler\n" +
			"";
		mono.android.Runtime.register ("CardSwipeLib.SwipeFlingAdapterViewTest/AdapterDataSetObserver, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", SwipeFlingAdapterViewTest_AdapterDataSetObserver.class, __md_methods);
	}


	public SwipeFlingAdapterViewTest_AdapterDataSetObserver () throws java.lang.Throwable
	{
		super ();
		if (getClass () == SwipeFlingAdapterViewTest_AdapterDataSetObserver.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterViewTest/AdapterDataSetObserver, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public SwipeFlingAdapterViewTest_AdapterDataSetObserver (md5feca4c7b78b30334560e51c2a0f57188.SwipeFlingAdapterViewTest p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == SwipeFlingAdapterViewTest_AdapterDataSetObserver.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterViewTest/AdapterDataSetObserver, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "CardSwipeLib.SwipeFlingAdapterViewTest, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", this, new java.lang.Object[] { p0 });
	}


	public void onChanged ()
	{
		n_onChanged ();
	}

	private native void n_onChanged ();


	public void onInvalidated ()
	{
		n_onInvalidated ();
	}

	private native void n_onInvalidated ();

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
