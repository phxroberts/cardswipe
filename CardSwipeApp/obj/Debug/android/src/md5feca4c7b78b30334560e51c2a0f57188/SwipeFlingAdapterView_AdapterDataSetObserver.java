package md5feca4c7b78b30334560e51c2a0f57188;


public class SwipeFlingAdapterView_AdapterDataSetObserver
	extends android.database.DataSetObserver
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onChanged:()V:GetOnChangedHandler\n" +
			"n_onInvalidated:()V:GetOnInvalidatedHandler\n" +
			"";
		mono.android.Runtime.register ("CardSwipeLib.SwipeFlingAdapterView/AdapterDataSetObserver, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", SwipeFlingAdapterView_AdapterDataSetObserver.class, __md_methods);
	}


	public SwipeFlingAdapterView_AdapterDataSetObserver () throws java.lang.Throwable
	{
		super ();
		if (getClass () == SwipeFlingAdapterView_AdapterDataSetObserver.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterView/AdapterDataSetObserver, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public SwipeFlingAdapterView_AdapterDataSetObserver (md5feca4c7b78b30334560e51c2a0f57188.SwipeFlingAdapterView p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == SwipeFlingAdapterView_AdapterDataSetObserver.class)
			mono.android.TypeManager.Activate ("CardSwipeLib.SwipeFlingAdapterView/AdapterDataSetObserver, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "CardSwipeLib.SwipeFlingAdapterView, CardSwipeLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", this, new java.lang.Object[] { p0 });
	}


	public void onChanged ()
	{
		n_onChanged ();
	}

	private native void n_onChanged ();


	public void onInvalidated ()
	{
		n_onInvalidated ();
	}

	private native void n_onInvalidated ();

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
