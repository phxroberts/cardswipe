﻿using System;
using Android.Views;
using Android.Graphics;
using Android.Views.Animations;
using Android.Animation;
using Android.Widget;
using Android.Content;
using Android.Util;


namespace CardSwipeLib
{
	public class BaseFlingAdapterView : AdapterView<IListAdapter> {

		private int heightMeasureSpec;
		private int widthMeasureSpec;

		public BaseFlingAdapterView (Context context) : base(context){

		}
		public BaseFlingAdapterView(Context context, IAttributeSet attrs) : base(context,attrs) {

		}
		public BaseFlingAdapterView(Context context, IAttributeSet attrs, int defStyle) 
			: base(context,attrs,defStyle) {

		}
		public int getWidthMeasureSpec() {
			return widthMeasureSpec;
		}

		public int getHeightMeasureSpec() {
			return heightMeasureSpec;
		}
		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
			this.widthMeasureSpec = widthMeasureSpec;
			this.heightMeasureSpec = heightMeasureSpec;
		}
		#region implemented abstract members of AdapterView

		public override void SetSelection (int position)
		{
			throw new NotImplementedException ();
		}

		public override View SelectedView {
			get {
				throw new NotImplementedException ();
			}
		}

		#endregion

		#region implemented abstract members of AdapterView

		public override IListAdapter Adapter {
			get {
				throw new NotImplementedException ();
			}
			set {
				throw new NotImplementedException ();
			}
		}

		#endregion

	}

}

