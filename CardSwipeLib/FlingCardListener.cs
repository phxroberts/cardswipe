﻿using System;
using Android.Views;
using Android.Graphics;
using Android.Views.Animations;
using Android.Animation;

namespace CardSwipeLib
{
	public interface IFlingListener {
		void onCardExited();

		void leftExit(Object dataObject);

		void rightExit(Object dataObject);

		void onClick(Object dataObject);

		void onScroll(float scrollProgressPercent);
	}

	public class FlingCardListener : Java.Lang.Object, View.IOnTouchListener
	{
		private static  String TAG = typeof(FlingCardListener).ToString();
		private const int INVALID_POINTER_ID = -1;

		private  float objectX;
		private  float objectY;
		private  int objectH;
		private  int objectW;
		private  int parentWidth;
		private  IFlingListener mFlingListener;
		private  Object dataObject;
		private  float halfWidth;
		private float BASE_ROTATION_DEGREES;

		private float aPosX;
		private float aPosY;
		private float aDownTouchX;
		private float aDownTouchY;

		// The active pointer is the one currently moving our object.
		private int mActivePointerId = INVALID_POINTER_ID;
		private View frame = null;


		private  int TOUCH_ABOVE = 0;
		private  int TOUCH_BELOW = 1;
		private int touchPosition;
		private  Object obj = new Object();
		public bool isAnimationRunning { get; set; }
		private float MAX_COS = (float) Math.Cos(ToRadians(45));


		public FlingCardListener(View frame, Object itemAtPosition, IFlingListener flingListener) 
			: this(frame, itemAtPosition, 15f, flingListener) {
			
		}

		public FlingCardListener(View frame, Object itemAtPosition, float rotation_degrees, IFlingListener flingListener) {
			isAnimationRunning = false;
			this.frame = frame;
			this.objectX = frame.GetX();
			this.objectY = frame.GetY();
			this.objectH = frame.Height;
			this.objectW = frame.Width;
			this.halfWidth = objectW / 2f;
			this.dataObject = itemAtPosition;
			this.parentWidth = ((ViewGroup) frame.Parent).Width;
			this.BASE_ROTATION_DEGREES = rotation_degrees;
			this.mFlingListener = flingListener;

		}

		private void processActionDownTouchEvent(View v, MotionEvent e) {
			// from http://android-developers.blogspot.com/2010/06/making-sense-of-multitouch.html
			// Save the ID of this pointer

			mActivePointerId = e.GetPointerId(0);
			float x = 0;
			float y = 0;
			bool success = false;
			try {
				x = e.GetX(mActivePointerId);
				y = e.GetY(mActivePointerId);
				success = true;
			} catch (Exception err) {
				//System.Diagnostics.Debug.WriteLine (notset);
				//Log.w(TAG, "Exception in onTouch(view, event) : " + mActivePointerId, e);
			}

			if (success) {
				// Remember where we started
				aDownTouchX = x;
				aDownTouchY = y;
				//to prevent an initial jump of the magnifier, aposX and aPosY must
				//have the values from the magnifier frame
				if (aPosX == 0) {
					aPosX = frame.GetX();
				}
				if (aPosY == 0) {
					aPosY = frame.GetY();
				}

				if (y < objectH / 2) {
					touchPosition = TOUCH_ABOVE;
				} else {
					touchPosition = TOUCH_BELOW;
				}
			}
			v.Parent.RequestDisallowInterceptTouchEvent (true);
		}
		private void processActionUpTouchEvent(View v, MotionEvent e) {
			mActivePointerId = INVALID_POINTER_ID;
			resetCardViewOnStack();
			v.Parent.RequestDisallowInterceptTouchEvent(false);
		}
		private void processActionCancelTouchEvent(View v, MotionEvent e) {
			mActivePointerId = INVALID_POINTER_ID;
			v.Parent.RequestDisallowInterceptTouchEvent(false);
		}
		private void processActionPointerUpTouchEvent(View v, MotionEvent e) {
			//int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
			int pointerIndex = (int) (e.Action & MotionEventActions.PointerIndexMask) >> (int) MotionEventActions.PointerIndexShift;
			int pointerId = e.GetPointerId(pointerIndex);
			if (pointerId == mActivePointerId) {
				// This was our active pointer going up. Choose a new
				// active pointer and adjust accordingly.
				int newPointerIndex = pointerIndex == 0 ? 1 : 0;
				mActivePointerId = e.GetPointerId(newPointerIndex);
			}
		}
		private void processActionMoveTouchEvent(View v, MotionEvent e) {
			// Find the index of the active pointer and fetch its position
			 int pointerIndexMove = e.FindPointerIndex(mActivePointerId);
			 float xMove = e.GetX(pointerIndexMove);
			 float yMove = e.GetY(pointerIndexMove);

			//from http://android-developers.blogspot.com/2010/06/making-sense-of-multitouch.html
			// Calculate the distance moved
			 float dx = xMove - aDownTouchX;
			 float dy = yMove - aDownTouchY;


			// Move the frame
			aPosX += dx;
			aPosY += dy;

			// calculate the rotation degrees
			float distobjectX = aPosX - objectX;
			float rotation = BASE_ROTATION_DEGREES * 2 * distobjectX / parentWidth;
			if (touchPosition == TOUCH_BELOW) {
				rotation = -rotation;
			}

			//in this area would be code for doing something with the view as the frame moves.
			frame.SetX(aPosX);
			frame.SetY(aPosY);
			frame.Rotation = rotation;
			mFlingListener.onScroll(getScrollProgressPercent());

		}
		public bool OnTouch (View v, MotionEvent e)
		{
			MotionEventActions action = e.Action & MotionEventActions.Mask;
			switch (action) {
			case MotionEventActions.Down:
				processActionDownTouchEvent (v, e);
				break;
			case MotionEventActions.Up:
				processActionUpTouchEvent (v, e);
				break;
			case MotionEventActions.Cancel:
				processActionCancelTouchEvent (v, e);
				break;
			case MotionEventActions.PointerUp:
				processActionPointerUpTouchEvent (v, e);
				break;
			case MotionEventActions.Move:
				processActionMoveTouchEvent (v, e);
				break;
			default:
				break;
			}

			return true;
		}
		private float getScrollProgressPercent() {
			if (movedBeyondLeftBorder()) {
				return -1f;
			} else if (movedBeyondRightBorder()) {
				return 1f;
			} else {
				float zeroToOneValue = (aPosX + halfWidth - leftBorder()) / (rightBorder() - leftBorder());
				return zeroToOneValue * 2f - 1f;
			}
		}
		private bool movedBeyondLeftBorder() {
			return aPosX + halfWidth < leftBorder();
		}

		private bool movedBeyondRightBorder() {
			return aPosX + halfWidth > rightBorder();
		}


		public float leftBorder() {
			return parentWidth / 4;
		}

		public float rightBorder() {
			return 3 * parentWidth / 4;
		}

		private bool resetCardViewOnStack() {
			
			if (movedBeyondLeftBorder()) {
				// Left Swipe
				onSelected(true, getExitPoint(-objectW), 100);
				mFlingListener.onScroll(-1.0f);
			} else if (movedBeyondRightBorder()) {
				// Right Swipe
				onSelected(false, getExitPoint(parentWidth), 100);
				mFlingListener.onScroll(1.0f);
			} else {
				float abslMoveDistance = Math.Abs(aPosX - objectX);
				aPosX = 0;
				aPosY = 0;
				aDownTouchX = 0;
				aDownTouchY = 0;
				frame.Animate ()
					.SetDuration (200)
					.SetInterpolator (new OvershootInterpolator (1.5f))
					.X (objectX)
					.Y (objectY)
					.Rotation (0);
				/*
				frame.animate()
					.setDuration(200)
					.setInterpolator(new OvershootInterpolator(1.5f))
					.x(objectX)
					.y(objectY)
					.rotation(0);
				*/
				mFlingListener.onScroll(0.0f);
				if (abslMoveDistance < 4.0) {
					mFlingListener.onClick(dataObject);
				}
			}

			return false;
		}

		class OnSelectedAnimatorListenerAdapter : AnimatorListenerAdapter {
			private bool m_IsLeft;
			private  IFlingListener mFlingListener;
			private  Object dataObject;
			private FlingCardListener m_FlingCardListener;
			public OnSelectedAnimatorListenerAdapter (bool isLeft, FlingCardListener flingCardListenerInstance, 
				IFlingListener flingListenerInstance, Object dataObjectInstance)
			{
				m_IsLeft=isLeft;
				m_FlingCardListener=flingCardListenerInstance;
				mFlingListener=flingListenerInstance;
				dataObject=dataObjectInstance;
			}
			public override void OnAnimationEnd (Animator animation)
			{
				//base.OnAnimationEnd (animation);
				if (m_IsLeft) {
					mFlingListener.onCardExited();
					mFlingListener.leftExit(dataObject);
				} else {
					mFlingListener.onCardExited();
					mFlingListener.rightExit(dataObject);
				}
				m_FlingCardListener.isAnimationRunning = false;
			}
		}

		public void onSelected(bool isLeft,
			float exitY, long duration) {

			isAnimationRunning = true;
			float exitX;
			if (isLeft) {
				exitX = -objectW - getRotationWidthOffset();
			} else {
				exitX = parentWidth + getRotationWidthOffset();
			}
			frame.Animate ()
				.SetDuration (duration)
				.SetInterpolator (new AccelerateInterpolator ())
				.X(exitX)
				.Y(exitY).SetListener(new OnSelectedAnimatorListenerAdapter(isLeft,this,mFlingListener,dataObject))
				.Rotation(getExitRotation(isLeft));
			/*
			this.frame.animate()
				.setDuration(duration)
				.setInterpolator(new AccelerateInterpolator())
				.x(exitX)
				.y(exitY)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						if (isLeft) {
							mFlingListener.onCardExited();
							mFlingListener.leftExit(dataObject);
						} else {
							mFlingListener.onCardExited();
							mFlingListener.rightExit(dataObject);
						}
						isAnimationRunning = false;
					}
				})
				.rotation(getExitRotation(isLeft));
			*/
		}



		public void selectLeft() {
			if (!isAnimationRunning)
				onSelected(true, objectY, 200);
		}


		public void selectRight() {
			if (!isAnimationRunning)
				onSelected(false, objectY, 200);
		}

		
		private float getExitPoint(int exitXPoint) {
			float[] x = new float[2];
			x[0] = objectX;
			x[1] = aPosX;

			float[] y = new float[2];
			y[0] = objectY;
			y[1] = aPosY;

			LinearRegression regression = new LinearRegression(x, y);

			//Your typical y = ax+b linear regression
			return (float) regression.slope() * exitXPoint + (float) regression.intercept();
		}

		private float getExitRotation(bool isLeft) {
			float rotation = BASE_ROTATION_DEGREES * 2 * (parentWidth - objectX) / parentWidth;
			if (touchPosition == TOUCH_BELOW) {
				rotation = -rotation;
			}
			if (isLeft) {
				rotation = -rotation;
			}
			return rotation;
		}


		private float getRotationWidthOffset() {
			return objectW / MAX_COS - objectW;
		}


		public void setRotationDegrees(float degrees) {
			this.BASE_ROTATION_DEGREES = degrees;
		}

		public bool isTouching() {
			return this.mActivePointerId != INVALID_POINTER_ID;
		}


		public PointF getLastPoint() {
			return new PointF(this.aPosX, this.aPosY);
		}




		private static float ToRadians (float degrees)
		{
			return (float) (degrees * (System.Math.PI/180.0));
		}
	}
}

