﻿using System;
using Android.Views;
using Android.Graphics;
using Android.Views.Animations;
using Android.Animation;
using Android.Widget;
using Android.Content;
using Android.Util;
using Android.Database;
using Android.Content.Res;
using Android.Runtime;

namespace CardSwipeLib
{
	public class SwipeFlingAdapterView : BaseFlingAdapterView
	{
		private int MAX_VISIBLE = 4;
		private int MIN_ADAPTER_STACK = 6;
		private float ROTATION_DEGREES = 15;

		private IListAdapter mAdapter;
		private int LAST_OBJECT_IN_STACK = 0;
		private onFlingListener mFlingListener;
		private AdapterDataSetObserver mDataSetObserver;
		private bool mInLayout = false;
		private View mActiveCard = null;
		private OnItemClickListener mOnItemClickListener;
		private FlingCardListener flingCardListener;
		private PointF mLastTouchPoint;

		public SwipeFlingAdapterView (Context context) : this(context, null) {

		}
		public SwipeFlingAdapterView(Context context, IAttributeSet attrs) 
			: this(context, attrs, Resource.Attribute.SwipeFlingStyle) {

		}
		public SwipeFlingAdapterView(Context context, IAttributeSet attrs, int defStyle) 
			: base(context,attrs,defStyle) {

			TypedArray a = context.ObtainStyledAttributes(attrs, Resource.Styleable.SwipeFlingAdapterView, defStyle, 0);
			MAX_VISIBLE = a.GetInt(Resource.Styleable.SwipeFlingAdapterView_max_visible, MAX_VISIBLE);
			MIN_ADAPTER_STACK = a.GetInt(Resource.Styleable.SwipeFlingAdapterView_min_adapter_stack, MIN_ADAPTER_STACK);
			ROTATION_DEGREES = a.GetFloat(Resource.Styleable.SwipeFlingAdapterView_rotation_degrees, ROTATION_DEGREES);
			a.Recycle();
		}

		public override View SelectedView {
			get {
				return mActiveCard;
			}
		}
		public override IListAdapter Adapter {
			get {
				return mAdapter;
			}
			set {
				if (mAdapter != null && mDataSetObserver != null) {
					mAdapter.UnregisterDataSetObserver(mDataSetObserver);
					mDataSetObserver = null;
				}
				mAdapter = value;
				if (mAdapter != null  && mDataSetObserver == null) {
					mDataSetObserver = new AdapterDataSetObserver (this);
					mAdapter.RegisterDataSetObserver(mDataSetObserver);
				}
			}
		}
		protected override void OnLayout (bool changed, int left, int top, int right, int bottom)
		{
			base.OnLayout (changed, left, top, right, bottom);

			if (mAdapter == null) {
				return;
			}
			mInLayout = true;
			int adapterCount = mAdapter.Count;

			if(adapterCount == 0) {
				RemoveAllViewsInLayout();
			}
			else {
				View topCard = GetChildAt(LAST_OBJECT_IN_STACK);
				if(mActiveCard!=null && topCard!=null && topCard==mActiveCard) {
					if (this.flingCardListener.isTouching()) {
						PointF lastPoint = this.flingCardListener.getLastPoint();
						if (this.mLastTouchPoint == null || !this.mLastTouchPoint.Equals(lastPoint)) {
							this.mLastTouchPoint = lastPoint;
							RemoveViewsInLayout(0, LAST_OBJECT_IN_STACK);
							layoutChildren(1, adapterCount);
						}
					}
				}
				else{
					// Reset the UI and set top view listener
					RemoveAllViewsInLayout();
					layoutChildren(0, adapterCount);
					setTopView();
				}

			}

			mInLayout = false;

			if(adapterCount <= MIN_ADAPTER_STACK) 
				mFlingListener.onAdapterAboutToEmpty(adapterCount);

		}

		private void layoutChildren(int startingIndex, int adapterCount){
			
			while (startingIndex < Math.Min (adapterCount, MAX_VISIBLE)) {
				View newUnderChild = mAdapter.GetView(startingIndex, null, this);
				if (newUnderChild.Visibility != ViewStates.Gone) {
					makeAndAddView(newUnderChild);
					LAST_OBJECT_IN_STACK = startingIndex;
				}
				startingIndex++;
			}
		}
		private void makeAndAddView(View child) {

			FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) child.LayoutParameters;
			AddViewInLayout(child, 0, lp, true);
		
			bool needToMeasure = child.IsLayoutRequested;

			if (needToMeasure) {
				int childWidthSpec = GetChildMeasureSpec (getWidthMeasureSpec (),
					PaddingLeft + PaddingRight + lp.LeftMargin + lp.RightMargin, lp.Width);
				int childHeightSpec = GetChildMeasureSpec(getHeightMeasureSpec(),
					PaddingTop + PaddingBottom + lp.TopMargin + lp.BottomMargin,lp.Height);
				child.Measure(childWidthSpec, childHeightSpec);
			} else {
				CleanupLayoutState(child);
			}


			int w = child.MeasuredWidth;
			int h = child.MeasuredHeight;

			GravityFlags gravity = lp.Gravity;
			if ((int)gravity == -1) {
				gravity = GravityFlags.Top | GravityFlags.Start;
			}

			GravityFlags layoutDirection = (GravityFlags)LayoutDirection;
			GravityFlags absoluteGravity = Gravity.GetAbsoluteGravity(gravity,layoutDirection);
			GravityFlags verticalGravity = gravity & GravityFlags.VerticalGravityMask;
			//int layoutDirection = getLayoutDirection();
			//int absoluteGravity = Gravity.getAbsoluteGravity(gravity, layoutDirection);
			//int verticalGravity = gravity & Gravity.VERTICAL_GRAVITY_MASK;

			int childLeft;
			int childTop;

			switch (absoluteGravity & GravityFlags.HorizontalGravityMask) {
			case GravityFlags.CenterHorizontal:
				childLeft = (Width + PaddingLeft - PaddingRight  - w) / 2 +
					lp.LeftMargin - lp.RightMargin;
				break;
			case GravityFlags.End:
				childLeft = Width + PaddingRight - w - lp.RightMargin;
				break;
			case GravityFlags.Start:
			default:
				childLeft = PaddingLeft + lp.LeftMargin;
				break;
			}

			switch (verticalGravity) {
			case GravityFlags.CenterVertical:
				childTop = (Height + PaddingTop - PaddingBottom  - h) / 2 +
					lp.TopMargin - lp.BottomMargin;
				break;
			case GravityFlags.Bottom:
				childTop = Height - PaddingBottom - h - lp.BottomMargin;
				break;
			case GravityFlags.Top:
			default:
				childTop = PaddingTop + lp.TopMargin;
				break;
			}

			child.Layout(childLeft, childTop, childLeft + w, childTop + h);

		}
		private void setTopView() {
			
			if(ChildCount>0){

				mActiveCard = GetChildAt(LAST_OBJECT_IN_STACK);
				if(mActiveCard!=null) {
					flingCardListener = new FlingCardListener (mActiveCard, mAdapter.GetItem (0),
						ROTATION_DEGREES, new FlingListener (this));
					//FlingListener
					/*
					flingCardListener = new FlingCardListener(mActiveCard, mAdapter.getItem(0),
						ROTATION_DEGREES, new FlingCardListener.FlingListener() {
							@Override
							public void onCardExited() {
								mActiveCard = null;
								mFlingListener.removeFirstObjectInAdapter();
							}
							@Override
							public void leftExit(Object dataObject) {
								mFlingListener.onLeftCardExit(dataObject);
							}
							@Override
							public void rightExit(Object dataObject) {
								mFlingListener.onRightCardExit(dataObject);
							}
							@Override
							public void onClick(Object dataObject) {
								if(mOnItemClickListener!=null)
									mOnItemClickListener.onItemClicked(0, dataObject);
							}
							@Override
							public void onScroll(float scrollProgressPercent) {
								mFlingListener.onScroll(scrollProgressPercent);
							}
						});
					*/
					mActiveCard.SetOnTouchListener(flingCardListener);

				}
			}

		}


		class FlingListener : IFlingListener{
			SwipeFlingAdapterView  m_SwipeFlingAdapterView;
			public FlingListener(SwipeFlingAdapterView swipeFlingAdapterViewInstance) {
				m_SwipeFlingAdapterView=swipeFlingAdapterViewInstance;
			}
			public void onCardExited() {
				m_SwipeFlingAdapterView.mActiveCard = null;
				m_SwipeFlingAdapterView.mFlingListener.removeFirstObjectInAdapter();
			}
			public void leftExit(Object dataObject) {
				m_SwipeFlingAdapterView.mFlingListener.onLeftCardExit(dataObject);
			}
			public void rightExit(Object dataObject) {
				m_SwipeFlingAdapterView.mFlingListener.onRightCardExit(dataObject);
			}
			public void onClick(Object dataObject) {
				if(m_SwipeFlingAdapterView.mOnItemClickListener!=null)
					m_SwipeFlingAdapterView.mOnItemClickListener.onItemClicked(0, dataObject);
			}
			public void onScroll(float scrollProgressPercent) {
				m_SwipeFlingAdapterView.mFlingListener.onScroll(scrollProgressPercent);
			}
		}


		public FlingCardListener getTopCardListener() {
			if(flingCardListener==null){
				throw new NullReferenceException();
			}
			return flingCardListener;
		}


		public void setMaxVisible(int MAX_VISIBLE){
			this.MAX_VISIBLE = MAX_VISIBLE;
		}

		public void setMinStackInAdapter(int MIN_ADAPTER_STACK){
			this.MIN_ADAPTER_STACK = MIN_ADAPTER_STACK;
		}


		public void setFlingListener(onFlingListener onFlingListener) {
			this.mFlingListener = onFlingListener;
		}

		public void setOnItemClickListener(OnItemClickListener onItemClickListener){
			this.mOnItemClickListener = onItemClickListener;
		}

		public override LayoutParams GenerateLayoutParams (IAttributeSet attrs)
		{
			return new FrameLayout.LayoutParams(Context, attrs);
		}
		public void RequestUpdateLayout() {
			if (!mInLayout) {
				RequestLayout ();
			}
		}

		class AdapterDataSetObserver : DataSetObserver {
			private SwipeFlingAdapterView m_SwipeFlingAdapterViewInstance;
			public AdapterDataSetObserver (SwipeFlingAdapterView swipeFlingAdapterViewInstance)
			{
				m_SwipeFlingAdapterViewInstance=swipeFlingAdapterViewInstance;
			}
			public override void OnChanged ()
			{
				m_SwipeFlingAdapterViewInstance.RequestUpdateLayout ();
			}
			public override void OnInvalidated ()
			{
				m_SwipeFlingAdapterViewInstance.RequestUpdateLayout ();
			}
		}
		public interface OnItemClickListener {
			void onItemClicked(int itemPosition, Object dataObject);
		}

		public interface onFlingListener {
			void removeFirstObjectInAdapter();
			void onLeftCardExit(Object dataObject);
			void onRightCardExit(Object dataObject);
			void onAdapterAboutToEmpty(int itemsInAdapter);
			void onScroll(float scrollProgressPercent);
		}
	}

}

